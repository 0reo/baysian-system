#include <iostream>
#include<fstream>
#include <stdlib.h>
#include <cstdlib>

using namespace std;
double P_A, P_B_A, P_A_B = 0.0;
int lines = 0;

double find_P_A(char piece[], char data[][21])//piece = A
{
    bool next = true;
    double output = 0.0;
        int count = 0;
    int count2 = 0;

    while (count != lines)
    {
        while (count2 < sizeof data[count])
        {
            if ((data[count][0] == piece[0] && data[count][2] == piece[1]) ||
            (data[count][4] == piece[0] && data[count][6] == piece[1]) ||
            (data[count][8] == piece[0] && data[count][10] == piece[1]))
            {
                //printf("%c",data[count][count2]);
                //printf("\n");
                next = true;
            }
            else
            {
                next = false;
            }
            count2++;
        }
        if (next)
            {
                output++;
            }
        count++;
        count2 = 0;

    }
    return output/lines+1;
}

double find_P_B_A(char piece[], string result, char data[][21])//piece = A, result = B
{
    double output = 0.0;;
    string r;
    int count = 0;
    int count2 = 0;
    bool next = true;

    while (count < lines)
    {
        r.clear();
        for (int i = 12; i < 22; i++)
        {
            r+=data[count][i];
        }
        r.resize(result.length());
        while (count2 < sizeof data[count])
        {
            if ((r == result) && //check result
            ((data[count][0] == piece[0] && data[count][2] == piece[1]) || //check king1
            (data[count][4] == piece[0] && data[count][6] == piece[1]) || //check rook1
            (data[count][8] == piece[0] && data[count][10] == piece[1]))) //check king2
            {
                next = true;
            }
            else
            {
                next = false;
            }
            count2++;
        }
        if (next)
        {
            output++;
        }
        count++;
        count2 = 0;
    }
        return output/lines+1;

}

double find_P_A_B(char data[][21], char k1[], char r1[], char k2[], string result)
{
    double king_P_A, king_P_B_A, rook_P_A, rook_P_B_A, king2_P_A, king2_P_B_A, king, rook, king2, output;

    king_P_A = find_P_A(k1, data);
    king_P_B_A = find_P_B_A(k1, result, data);
    king = king_P_A * king_P_B_A;

    rook_P_A = find_P_A(r1, data);
    rook_P_B_A =   find_P_B_A(r1, result, data);
    rook = rook_P_A * rook_P_B_A;

    king2_P_A = find_P_A(k2, data);
    king2_P_B_A =  find_P_B_A(k2, result, data);
    king2 = king2_P_A * king2_P_B_A;

    output = king * rook* king2;

    return output;

}

void randomize(char inital[][21], char final[][21])
{
    //int highest=28056;
    int rcount = (lines*.6)+1;
    int rrcount = 0;
    int rans[]={1};
    int ran;

    bool t = true;
    char move[21];

    while (rcount > 0)
    {
        ran = rand()%lines;
        if (rans[ran])
        {
            t = false;
        }
        else
        {
            rans[ran] = 0;
        }

        if (t == true)
        {
            rcount--;
            for (int i = 0; i < 21; i++)
            {
                final[rcount][i] = inital[ran][i];
            }
        }
        else
        {
                t = true;
        }
    }

}

int main()
{
    ifstream krk;
    krk.open("krk.data");
    char pieces[4][2];
    //char result[8];
    string result;
    int count = 0;

    char output[30000][21];
    char ran[30000][21];
    char training;
    char validation;
    //char[] myFile[1000];

    if (krk.is_open())
    {
        //while (fgets (output, sizeof output, krk) != NULL)
        while (!krk.eof())
        {
            //krk.getline(output[count], 21);
            krk >> output[lines];
            //printf("%s\n",output[count]);
            lines++;
        }
    }
    krk.close();
    count = 0;

    cout << "enter position for white king\n";
    cin >> pieces[0];
    cout <<"enter position for white rook\n";
    cin >> pieces[1];
    cout <<"enter position for black king\n";
    cin >> pieces[2];
    cout <<"enter end result\n";
    cin >>result;
    P_A_B = find_P_A_B(ran, pieces[0], pieces[1], pieces[2], result);
    cout << fixed << P_A_B;
    return 0;
}

